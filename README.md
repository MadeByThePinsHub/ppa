# PPAs in GitLab Pages + Git LFS

## Why GitLab Pages?

Currently, the Debian package registry on GitLab is still on its way to production, so in meanwhile, please track the work
at <https://gitlab.com/groups/gitlab-org/-/epics/6057>.

## Adding new packages

We use Git LFS to store packages without the hassle for others to download it as part of the Git history. You need to install LFS on your machine before proceeding.

1. After cloning the repo, add the deb files you need to `public` directory.
2. Let Git LFS track that file with `git lfs track public/*.deb`.
3. Stage the file pointers and changes in the `.gitattributes` file with `git add public/ .gitattributes`.
4. Commit and push!

## References

* https://assafmo.github.io/2019/05/02/ppa-repo-hosted-on-github.html (we use GitLab Pages for that and Git LFS)
