#!/bin/bash

echo "===> Downloading release GPG keys..."
curl -fsSL https://madebythepinshub.gitlab.io/ppa/releases-key.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/thepinsteam-ppa-archive-keyring.gpg >> /dev/null
curl -fsSL https://cli.github.com/packages/githubcli-archive-keyring.gpg | sudo tee /usr/share/keyrings/github-cli-archive-keyring.gpg >> /dev/null

echo "===> Downloading repository list file..."
sudo wget -O /etc/apt/sources.list.d/recaptime-dev-ppa-starter-pack.list https://madebythepinshub.gitlab.io/ppa/repos.list
